package com.animoz.controleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.animoz.service.EspeceService;

@Controller
public class EspeceControleur {
	
	@Autowired
	private EspeceService especeService;
	
	@GetMapping("/espece")
	public String getEspeces(Model model, @ModelAttribute EspeceDto especeDto) {
		model.addAttribute("especes", especeService.getEspeces());
		return "especes";
	}
	
	@PostMapping("/espece")
	public String ajouterEspeces(@ModelAttribute EspeceDto especeDto) {
		especeService.ajouter(especeDto.getNom());
		return "redirect:/espece";
	}
	
}
