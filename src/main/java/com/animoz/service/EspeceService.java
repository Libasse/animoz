package com.animoz.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.animoz.dao.EspeceDao;
import com.animoz.modele.Espece;

@Service
public class EspeceService {
	
	@Autowired
	private EspeceDao especeDao;

	public List<Espece> getEspeces() {
		return especeDao.getEspeces();
	}

	@Transactional
	public void ajouter(String nom) {
		if(! especeDao.existe(nom)) {
			Espece espece = new Espece();
			espece.setNom(nom);
			especeDao.ajouter(espece);
		}
	}
}
